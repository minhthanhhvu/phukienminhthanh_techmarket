<?php
/**
 * All Dokan Related functions
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! function_exists( 'techmarket_dokan_scripts' ) ) {
	/**
	 * Enqueue Dokan Scripts
	 */
	function techmarket_dokan_scripts() {
		global $techmarket_version;
		wp_enqueue_style( 'techmarket-dokan-style', get_template_directory_uri() . '/assets/css/dokan/dokan.css', '', $techmarket_version );
	}
}

if ( ! function_exists( 'techmarket_dokan_store_layout' ) ) {
	/**
	 * Dokan Store Layout
	 */
	function techmarket_dokan_store_layout() {
		$layout = apply_filters( 'techmarket_dokan_store_layout', 'full-width' );

		return $layout;
	}
}

if ( ! function_exists( 'techmarket_dokan_shop_layout' ) ) {
	/**
	 * Dokan Store Layout
	 */
	function techmarket_dokan_shop_layout( $layout ) {
		if( dokan_is_store_page() ) {
			$layout = techmarket_dokan_store_layout();
		}

		return $layout;
	}
}

if( ! function_exists( 'techmarket_dokan_body_classes' ) ) {
	function techmarket_dokan_body_classes( $classes ) {
		if( dokan_is_store_page() ) {
			$blog_layout = techmarket_get_blog_layout();
			if( ( $key = array_search( $blog_layout, $classes ) ) !== false ) {
				unset($classes[$key]);
			}

			$classes[] = techmarket_dokan_store_layout();
		}

		return $classes;
	}
}