<?php
/**
 * Additional functions used by the theme
 */

if ( ! function_exists( 'pr' ) ) {
	function pr( $var ) {
		echo '<pre>' . print_r( $var, 1 ) . '</pre>';
	}
}