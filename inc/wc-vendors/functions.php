<?php
/**
 * All WC Vendors Related functions
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! function_exists( 'techmarket_wc_vendors_scripts' ) ) {
	/**
	 * Enqueue WC Vendors Scripts
	 */
	function techmarket_wc_vendors_scripts() {
		global $techmarket_version;
		wp_enqueue_style( 'techmarket-wc-vendors-style', get_template_directory_uri() . '/assets/css/wc-vendors/wc-vendors.css', '', $techmarket_version );
	}
}