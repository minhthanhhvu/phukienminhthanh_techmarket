<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

$product_id = $product->get_id();
$specifications = get_post_meta( $product_id, '_specifications', true );
$specifications_display_attributes = get_post_meta( $product_id, '_specifications_display_attributes', true );

if ( $specifications_display_attributes == 'yes' && ( $product->has_attributes() || ( apply_filters( 'wc_product_enable_dimensions_display', true ) && ( $product->has_dimensions() || $product->has_weight() ) ) ) ) {
	$attributes_title = get_post_meta( $product_id, '_specifications_attributes_title', true );
	if ( $attributes_title ) {
		echo wp_kses_post( '<h2>' . $attributes_title . '</h2>' );
	}

	wc_display_product_attributes( $product );
}

echo apply_filters( 'the_content', wp_kses_post( $specifications ) );